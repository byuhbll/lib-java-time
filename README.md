[![Build Status](https://semaphoreci.com/api/v1/projects/24c686b3-a00d-40f8-b310-849d585eabe0/1192078/shields_badge.svg)](https://semaphoreci.com/byuhbll/lib-java-time)
# Java Time Support Library

This library provides support classes for working with the new Java 8 time library.

## FlexTime

The **FlexTime** class acts as a proxy for a preconfigured **DateTimeFormatter** to easily parse an ISO-8601 date
or date fragment.  This functionality becomes useful in several contexts, such as when building an API which requires
a date parameter.  The following temporal types are supported:

- **Year**: `YYYY`
- **YearMonth**: `YYYY-MM`
- **LocalDate**: `YYYY-MM-DD`
- **LocalDateTime**: `YYYY-MM-DDTHH:MM`, `YYYY-MM-DDTHH:MM:SS`, `YYYY-MM-DDTHH:MM:SS.NNNNNNNNN`
- **OffsetDateTime**, **ZonedDateTime**: `YYYY-MM-DDTHH:MM±ZZ:ZZ`, `YYYY-MM-DDTHH:MM:SS±ZZ:ZZ`,
`YYYY-MM-DDTHH:MM:SS.NNNNNNNNN±ZZ:ZZ` (The use of the shorthand `Z` to denote UTC or "Zulu" time instead of a timezone
declaration is also allowed)  

To use this flexible parsing functionality, simply make a static call to the `parseBest` method:

```java
// Equivalent to LocalDate.parse("2015-01-15");
TemporalAccessor localDate = FlexTime.parseBest("2015-01-15");

// Equivalent to ZonedDateTime.parse("2015-01-15T00:01:02-06:00");
TemporalAccessor zonedDateTime = FlexTime.parseBest("2015-01-15T00:01:02-06:00");
```

Since the **TemporalAccessor** interface does not provide much useful functionality on its own, users may find it useful
to couple this method with the `firstInstant` or `lastInstant` methods also available in **FlexTime**, which will
convert the temporal types listed above to **Instant**s.  For temporal types representing a single instant on the
timeline, this conversion is straightforward; for temporal types representing a period of time instead (such as a day,
month, or year), the first or last instant belonging to that period of time will be returned respectively.

```java
// Equivalent to LocalDateTime.parse("2015-01-15T00:00:00").atZone(ZoneId.systemDefault()).toInstant();
Instant instant = FlexTime.firstInstant(FlexTime.parseBest("2015-01-15");

// Equivalent to ZonedDateTime.parse("2015-01-15T00:01:02-06:00").toInstant();
Instant instant = FlexTime.lastInstant(FlexTime.parseBest("2015-01-15T00:01:02-06:00");
```

Since version 1.1.0, the `parse` method(s) further simplify the process of parsing **String**s into **Instant**s,
even `null` or empty Strings, making it even easier to use **FlexTime** to handle date/time conversions coming from
a web service or a backend data store:

```java
Instant a = FlexTime.parse(null);
// a == null

Instant b = FlexTime.parse("", FlexTime::firstInstant);
// b == null

Instant c = FlexTime.parse("2015-01-15", ZoneId.of("UTC"));
// c == 2015-01-15T00:00:00Z

Instant instant = FlexTime.parse("2015-01-15", ZoneId.of("UTC"), FlexTime::lastInstant);
// d == 2015-01-15T23:59:59.999999999Z
```

## BYU Holidays and Temporal Adjusters

The **ByuHolidays** and **ByuTemporalAdjusters** classes offer methods which calculate the dates of Brigham Young
University (BYU) university holidays for a given year.

Two methods in **ByuHolidays**, `universityHolidays` and `isWorkingDay`, take advantage of an internal cache and will
return very efficiently for repeated calls within a single year.  Other methods will recalculate the corresponding
holidays each time they are called.

```java
// Returns all university holidays for the year 2015.
List<LocalDate> holidays2015 = ByuHolidays.universityHolidays(2015);

// Evaluates whether a given date is a working/business day at BYU.
boolean isWorkingDay = ByuHolidays.isWorkingDay(LocalDate.parse("2015-01-12"));
```

The **ByuTemporalAdjusters** utility class offers a collection of **TemporalAdjuster**s to complement the
**TemporalAdjusters** class provided in the Java standard libraries.  The `never` adjuster always returns the maximum
possible value for a given date or date/time, a point in time occurring in the year 1 Billion AD.  The `plusWorkingDays`
and `minusWorkingDays` adjusters use the previously described BYU holiday information to provide a simple mechanism for
identifying a date occuring some N number of business/working days (i.e.: excluding weekends and
holidays) in the future or past.

```java
// Returns 2015-01-22. January 15, 2015 is the Friday prior to Martin Luther King Jr. Day (a university holiday). As a
// result, January 16 (Saturday), 17 (Sunday), and 18 (Holiday) are all skipped when calculating the date adjustment.
LocalDate date = LocalDate.parse("2015-01-15").with(ByuTemporalAdjusters.plusWorkingDays(4));
```